package ro.aniela;

public class Exercise17 {

    public static void main(String[] args) {
        int[] argsss = {1, 3, 4, 7, 9};
       System.out.println(largestNumber(argsss));

    }

    private static int largestNumber(int[] args) {
        int largestNumber = args[0];
        for (int i : args) {
            if (i > largestNumber) {
                largestNumber = i;
            }
        }
        return largestNumber;
    }
}

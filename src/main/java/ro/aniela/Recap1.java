package ro.aniela;

public class Recap1 {
    public static void main(String[] args) {

        System.out.println(weekName(6));
        nameOfMonth(8);
    }

    private static String weekName(int day) {
        switch (day) {
            case 1:
                return "Monday";
            case 2:
                return "Tueday";
            case 3:
                return "Wednesay";
            case 4:
                return "Tuesday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                return "This is not a day";
        }
    }

    private static void nameOfMonth(int month) {
        switch (month) {
            case 1:
                System.out.println("j");
                break;
            case 2:
                System.out.println("f");
                break;
            case 3:
                System.out.println("mr");
                break;
            default:
                System.out.println("l");

        }
    }

    private static int numberOfDays(String month) {
        switch (month) {
            case "january":
            case "'fe":
            case "n":
                return 31;

            case "j":
            case "c":
                return 30;
            default:
                return 7;
        }
    }
}

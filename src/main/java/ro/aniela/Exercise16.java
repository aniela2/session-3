package ro.aniela;

public class Exercise16 {

    public static void main(String[] args) {
        int[] argss = {1, 2, 3, 4, 5};
        System.out.println(oddNumbers(argss));

    }

    private static int oddNumbers(int[] args) {
        int sum = 0;
        for (int i : args) {
            if (i % 2 == 1) {
                sum += i;
            }
        }
        return sum;
    }
}
package ro.aniela;

public class Exercise9 {
    public static void main(String[] args) {
        double[] numbers = {1d, 2d, 3d};
        System.out.println(sumOfAllNumbers(numbers));
        System.out.println(normalFor(numbers));
    }

    private static double sumOfAllNumbers(double[] numbers) {
        double sum = 0d;

        for (double number : numbers) {
            sum = sum + number;
        }
        return sum;
    }

    private static double normalFor(double[] numbers) {

        double sum = 0d;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];

        }
        return sum;

    }
}

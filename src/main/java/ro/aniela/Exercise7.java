package ro.aniela;

public class Exercise7 {

    public static void main(String[] args) {
        naturalNumbers(2, 4);

    }

    private static void naturalNumbers(int x, int y) {
        for (int i = x; i <= y; i++) {
            System.out.println(i);
        }
    }
}

package ro.aniela;

public class Exercisses3 {

    public static void main(String[] args) {
        System.out.println(daysInMonth("March"));

    }

    private static int daysInMonth(String x) {

        switch (x) {
            case "January":
            case "March":
            case "May":
            case "July":
            case "august":
            case "october":
            case "decembrie":
                return 31;
            case "february":
                return 28;
            case "April":
            case "june":
            case "September":
            case "november":
                return 30;
            default:
                return 0;


        }
    }
}

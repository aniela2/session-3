package ro.aniela;

public class Exercise11 {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};
        largestNumber(numbers);

    }

    private static void largestNumber(int[] numbers) {
        int greatest = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > greatest) {
                greatest = numbers[i];
            }
        }
        System.out.println(greatest);
    }

 }


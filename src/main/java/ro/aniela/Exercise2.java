package ro.aniela;

public class Exercise2 {
    public static void main(String[] args) {
        months(1);
    }

    private static void months(int y) {
        switch (y) {
            case 1:
                System.out.println("j");
                break;
            case 2:
                System.out.println("f");
                break;
            case 3:
                System.out.println("m");
                break;
            case 4:
                System.out.println("a");
                break;
            case 5:
                System.out.println("may");
                break;
            case 6:
                System.out.println("june");
                break;
            case 7:
                System.out.println("july");
                break;
            case 8:
                System.out.println("august");
                break;
            case 9:
                System.out.println("s");
                break;
            case 10:
                System.out.println("o");
                break;
            case 11:
                System.out.println("n");
                break;
            case 12:
                System.out.println("d");
                break;
            default:
                System.out.println("Is not a month");

        }

    }

}

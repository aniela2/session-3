package ro.aniela;

public class Exercise15 {
    public static void main(String[] args) {

        System.out.println(sumNumbers(5, 2, 50));

    }

    private static int sumNumbers(int x, int to, int from) {
        int sum = 0;
        for (int i = to; i <= from; i++) {
            if (i % x == 0) {
                sum = sum + i;
            }
        }
        return sum;

    }
}

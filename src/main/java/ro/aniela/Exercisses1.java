package ro.aniela;

public class Exercisses1 {

    public static void main(String[] args){

        System.out.println(weekDay(7));

    }

    private static String weekDay(int x){
        switch(x){
            case 1 :
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "thursday";
            case 5:
                return "FRiday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
            return "This is not a day";


        }
    }
}

package ro.aniela;

public class Exercisses5 {

    public static void main(String[] args) {
        gender("male");
    }

    private static void gender(String x) {
        switch (x) {
            case "female":
                System.out.println("2");
                break;
            case "male":
                System.out.println("1");
                break;
            default:
                System.out.println(0);
        }

    }
}


package ro.aniela;

public class Exercise13 {

    public static void main(String[] args) {

        divisibleNumber(1, 100, 3, 5);


    }

    private static void divisibleNumber(int from, int to, int divisor1, int divisor2) {
        boolean isDivisorOne = false;
        boolean isDivisibleTwo = false;

        for (int i = from; i <= to; i++) {
            isDivisorOne = i % divisor1 == 0;
            isDivisibleTwo = i % divisor2 == 0;

            if (isDivisorOne && isDivisibleTwo || isDivisorOne || isDivisibleTwo) {
                System.out.println(i);
            }
        }

    }
}


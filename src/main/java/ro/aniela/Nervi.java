package ro.aniela;

public class Nervi {
    public static void main(String[] args) {
        divisible(1, 100, 3, 5);
        reverseNumbers(1, 100);
        System.out.println(sumNumberInARange(1,10,5));

//Write a Java program to print numbers between 1 to 100 which are divisible by 3, 5 or by both.
    }

    private static void divisible(int from, int to, int x, int y) {
        for (int i = from; i <= to; i++) {
            if ((i % x == 0 && i % y == 0) || i % x == 0 || i % y == 0) {
                System.out.println(i);

            }

        }

    }

    //Write a program to print all natural numbers within a range in reverse
    private static void reverseNumbers(int from, int to) {
        for (int i = to; i >= from; i--) {
            System.out.println(i);
        }

    }
    //Given a number, write a program to sum of all natural numbers within a range which are divisible by the given number.

    private static int sumNumberInARange(int from , int to, int number){

        int sum =0;
        for (int i =from; i<=to; i++){
            if(i%number==0){
                sum+=i;
            }

        }
        return sum ;
    }
}

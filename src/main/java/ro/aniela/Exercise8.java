package ro.aniela;

public class Exercise8 {

    public static void main(String[] args) {
        System.out.println(sumOfNumbers(2,4));

    }

    private static int sumOfNumbers(int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum = sum + i;
        }
        return sum;
    }

}

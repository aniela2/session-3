package ro.aniela;

public class Exercise18 {
    public static void main(String[] args) {
        System.out.println(sumOfNumbers(1, 10));

    }

    private static int sumOfNumbers(int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += i;
        }
        return sum;
    }
}

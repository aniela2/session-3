package ro.aniela;

public class Exercise12 {
    public static void main(String[] args) {

        System.out.println(divisible(2,10,5));

    }

    private static int divisible(int from, int to, int div) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            if (i % div == 0) {
                sum++;
            }

        }
        return sum;
    }

}

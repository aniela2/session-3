package ro.aniela;

public class Recap2 {

    public static void main(String[] args) {

        System.out.println(signs('/', 1, 2));
        gens("mal");
        naturalNumbers(1, 10);
        System.out.println(sumOfNumbers(1, 10));
        double[]x={1d,2d,3d};
        System.out.println(sumOfD(x));
    }

    private static int signs(char operator, int number1, int number2) {

        switch (operator) {
            case '+':
                return number1 + number2;
            case '-':
                return number1 - number2;
            case '*':
                return number1 * number2;
            default:
                return 0;
        }
    }

    private static void gens(String gen) {

        switch (gen) {

            case "male":
                System.out.println("1");
                break;
            case "female":
                System.out.println("2");
                break;
            default:
                System.out.println(0);
        }
    }

    private static void naturalNumbers(int from, int to) {
        for (int i = from; i <= to; i++) {
            System.out.println(i);

        }

    }

    private static int sumOfNumbers(int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum = sum + i;
        }
        return sum;
    }

    private static double sumOfD(double[] numbers) {
        double sum = 0d;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }
        return sum;
    }
}




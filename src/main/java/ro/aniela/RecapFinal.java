package ro.aniela;

public class RecapFinal {
    public static void main(String[]args){
        int []y={1,2,3,4};
        System.out.println(oddNumber(y));

    }
    //Given an array of natural numbers, write a program to sum all odd numbers from the given array.
    private static int oddNumber(int[] args){

        int sum=0;
        for (int i: args){
            if(i%2==0){
                sum+=i;
            }
        }
        return sum;
    }
}
